package pro.efiproj.moikt.assignment.domain.document;

import lombok.Getter;
import lombok.Setter;
import pro.efiproj.moikt.assignment.domain.company.Company;

import java.time.LocalDateTime;
import java.util.Objects;

/**
 * This class represents the document abstraction.
 * As there is no real database (although there is an interface
 * of spring data-jpa) this class acts as dto and entity at the
 * same time.
 *
 * @see pro.efiproj.moikt.assignment.repository.CrudRepository
 */
@Getter
@Setter
public class Document {
    /**
     * Creation date of the document. Needed for some restriction checks
     */
    private final LocalDateTime creationDate = LocalDateTime.now();

    private Long id;
    /**
     * Creator of the document
     */
    private Company creator;

    /**
     * Other company, whose sign is needed
     */
    private Company other;

    private boolean signedByCreator;
    private boolean signedByOther;

    /**
     * Status representing the state of
     * document's processing
     *
     * @see DocumentProcessStatus
     */
    private DocumentProcessStatus status = DocumentProcessStatus.NEWLY_CREATED;

    private String documentText;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Document document = (Document) o;
        return signedByCreator == document.signedByCreator &&
                signedByOther == document.signedByOther &&
                id.equals(document.id) &&
                creator.equals(document.creator) &&
                other.equals(document.other) &&
                status == document.status &&
                documentText.equals(document.documentText);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, creator, other, signedByCreator, signedByOther, status, documentText);
    }
}
