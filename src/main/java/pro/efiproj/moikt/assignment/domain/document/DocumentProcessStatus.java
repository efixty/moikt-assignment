package pro.efiproj.moikt.assignment.domain.document;

/**
 * Enum representing the status of the document
 *
 * @see Document#status
 */
public enum DocumentProcessStatus {
    /**
     * Assigned to document upon creation.
     */
    NEWLY_CREATED,

    /**
     * Assigned to document upon first sign on it.
     * In general its done automatically after creation
     * (the exceptions are cases with restrictions)
     */
    IN_PROCESS,

    /**
     * Assigned to the document when both companies signed it.
     * If document has this status then no operation can be done on it
     */
    SIGNED_BY_BOTH,
}
