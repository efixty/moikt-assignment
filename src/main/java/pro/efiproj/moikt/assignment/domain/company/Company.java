package pro.efiproj.moikt.assignment.domain.company;

import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

/**
 * This class represents the company abstraction.
 * As there is no real database (although there is an interface
 * of spring data-jpa) this class acts as dto and entity at the
 * same time.
 *
 * @see pro.efiproj.moikt.assignment.repository.CrudRepository
 */
@Getter
@Setter
public class Company {
    private Long id;
    private String name;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Company company = (Company) o;
        return id.equals(company.id) &&
                name.equals(company.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
}
