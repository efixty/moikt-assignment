package pro.efiproj.moikt.assignment.domain.restriction.impl;

import lombok.Getter;
import lombok.Setter;
import pro.efiproj.moikt.assignment.domain.company.Company;
import pro.efiproj.moikt.assignment.domain.document.Document;
import pro.efiproj.moikt.assignment.domain.restriction.Restriction;
import pro.efiproj.moikt.assignment.domain.restriction.RestrictionContext;
import pro.efiproj.moikt.assignment.domain.restriction.RestrictionResponse;
import pro.efiproj.moikt.assignment.domain.restriction.RestrictionType;
import pro.efiproj.moikt.assignment.repository.document.DocumentRepository;

import java.util.List;

/**
 * Restriction on document creation with participation of given signer.
 * There is a limit for every company to participate in processing of
 * documents that don't have SIGNED_BY_BOTH status
 *
 * @see RestrictionContext#ACTIVE_DOCUMENT_PROCESSING_DEFAULT_LIMIT
 * @see RestrictionContext#activeDocumentProcessingLimit
 */
@Getter
@Setter
public class ActiveDocumentProcessingLimit extends Restriction {

    @Override
    public RestrictionResponse isAllowed(RestrictionContext context, Document document, Company signer, DocumentRepository repository) {
        List<Document> documents = repository.findNotSignedByBothWithGivenSigner(signer.getId());
        int limit = context.getActiveDocumentProcessingLimit();
        boolean allowed = documents.size() < limit;
        if (!allowed) {
            final String unformattedMessage = bundle.getString("document.sign.active.document.limit");
            return RestrictionResponse.builder()
                    .allowed(false)
                    .message(String.format(unformattedMessage, signer.getId()))
                    .type(RestrictionType.ACTIVE_DOCUMENT_PROCESSING_RESTRICTION)
                    .build();
        }
        return checkNext(context, document, signer, repository);
    }
}
