package pro.efiproj.moikt.assignment.domain.restriction;

import lombok.Builder;
import lombok.Getter;

/**
 * Data class used as feedback from chain of restrictions
 * This class follows 5 immutability rules and uses builder pattern
 */
@Getter
@Builder
public final class RestrictionResponse {
    /**
     * Message with details. Can be set
     * as an error message in exception or
     * be logged
     */
    private final String message;

    /**
     * The actual response of chain.
     * <code>true</code> if everything was OK.
     * <code>false</code> if any of validations fail.
     */
    private final boolean allowed;

    /**
     * Type of restriction whose validation
     * was not passed. Used for correct further
     * handling. For example in case of
     * {@link RestrictionType#TIME_RESTRICTION} service
     * can schedule a task for documents with status
     * {@link pro.efiproj.moikt.assignment.domain.document.DocumentProcessStatus#NEWLY_CREATED}
     * to be automatically signed by creator as soon as as possible
     */
    private final RestrictionType type;
}
