package pro.efiproj.moikt.assignment.domain.restriction.impl;

import lombok.Getter;
import lombok.Setter;
import pro.efiproj.moikt.assignment.domain.company.Company;
import pro.efiproj.moikt.assignment.domain.document.Document;
import pro.efiproj.moikt.assignment.domain.restriction.Restriction;
import pro.efiproj.moikt.assignment.domain.restriction.RestrictionContext;
import pro.efiproj.moikt.assignment.domain.restriction.RestrictionResponse;
import pro.efiproj.moikt.assignment.domain.restriction.RestrictionType;
import pro.efiproj.moikt.assignment.repository.document.DocumentRepository;

import java.util.List;

/**
 * Restriction on document creation with same companies involved
 * There is a limit for documents that don't have SIGNED_BY_BOTH
 * status but have same signers
 *
 * @see RestrictionContext#SAME_COMPANY_DOCUMENT_PROCESSING_DEFAULT_LIMIT
 * @see RestrictionContext#sameCompanyDocumentProcessingLimit
 */
@Getter
@Setter
public class SameCompanyDocumentProcessingLimit extends Restriction {

    @Override
    public RestrictionResponse isAllowed(RestrictionContext context, Document document, Company signer, DocumentRepository repository) {
        List<Document> documents = repository.findNotSignedByBothByDocument(document);
        int limit = context.getSameCompanyDocumentProcessingLimit();
        boolean allowed = documents.size() < limit;
        if (!allowed) {
            final String unformattedMessage = bundle.getString("document.sign.same.company.restriction");
            return RestrictionResponse.builder()
                    .allowed(false)
                    .message(String.format(unformattedMessage,
                            document.getId(),
                            document.getCreator().getId(),
                            document.getOther().getId())
                    )
                    .type(RestrictionType.SAME_COMPANY_DOCUMENT_PROCESSING_LIMIT)
                    .build();
        }
        return checkNext(context, document, signer, repository);
    }
}
