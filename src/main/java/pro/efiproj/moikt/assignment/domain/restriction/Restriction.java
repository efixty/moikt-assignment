package pro.efiproj.moikt.assignment.domain.restriction;

import pro.efiproj.moikt.assignment.config.ResourceBundleProvider;
import pro.efiproj.moikt.assignment.domain.company.Company;
import pro.efiproj.moikt.assignment.domain.document.Document;
import pro.efiproj.moikt.assignment.repository.document.DocumentRepository;

import java.util.ResourceBundle;

/**
 * Abstract restriction. Part of 'chain of responsibility'
 * pattern implementation, that is used in restrictions
 * mechanism in the app. Any custom restriction must extend
 * this class in order to be able to be added to chain
 *
 * @see pro.efiproj.moikt.assignment.domain.restriction.impl.ActiveDocumentProcessingLimit
 * @see pro.efiproj.moikt.assignment.domain.restriction.impl.DocumentCreationLimitRestriction
 * @see pro.efiproj.moikt.assignment.domain.restriction.impl.DocumentProcessingTimeRestriction
 * @see pro.efiproj.moikt.assignment.domain.restriction.impl.SameCompanyDocumentProcessingLimit
 */
public abstract class Restriction {

    /**
     * Error message bundle, containing needed messages for all restrictions
     */
    protected final ResourceBundle bundle = ResourceBundleProvider.getResourceBundleForName("error_messages");

    /**
     * Next restriction in chain
     */
    private Restriction next;

    /**
     * Method for adding a new restriction to existing chain
     *
     * @param next to be added
     * @return newly added restriction for method chaining
     */
    public Restriction addNext(Restriction next) {
        this.next = next;
        return this.next;
    }

    /**
     * Checks if 'document' can be created or signed by 'signer' for given state of database
     * (accessed through given repository) with given restriction context.
     *
     * @param context    containing rules for validation
     * @param document   to be created/signed
     * @param signer     creator/signer of given document
     * @param repository of documents
     * @return restriction response containing some feedback about validation process
     */
    public abstract RestrictionResponse isAllowed(RestrictionContext context, Document document, Company signer, DocumentRepository repository);

    /**
     * If somewhere in chain the result is already known then response
     * is constructed and returned. Otherwise this method is called to
     * pass validation process to the next in chain. If 'next' is null
     * then the end of the chain reached and everything is ok.
     * If 'next' is not null then parameters are just passed to its
     * 'isAllowed' method and its returned response is returned.
     *
     * @param context    containing rules for validation
     * @param document   to be created/signed
     * @param signer     creator/signer of given document
     * @param repository of documents
     * @return 'OK' restriction response if next is null, otherwise returned response of next in chain
     */
    protected RestrictionResponse checkNext(RestrictionContext context, Document document, Company signer, DocumentRepository repository) {
        if (next == null) {
            return RestrictionResponse.builder()
                    .allowed(true)
                    .message(bundle.getString("restriction.ok"))
                    .type(RestrictionType.OK)
                    .build();
        }
        return next.isAllowed(context, document, signer, repository);
    }
}
