package pro.efiproj.moikt.assignment.domain.restriction.impl;

import lombok.Getter;
import lombok.Setter;
import pro.efiproj.moikt.assignment.domain.company.Company;
import pro.efiproj.moikt.assignment.domain.document.Document;
import pro.efiproj.moikt.assignment.domain.restriction.Restriction;
import pro.efiproj.moikt.assignment.domain.restriction.RestrictionContext;
import pro.efiproj.moikt.assignment.domain.restriction.RestrictionResponse;
import pro.efiproj.moikt.assignment.domain.restriction.RestrictionType;
import pro.efiproj.moikt.assignment.repository.document.DocumentRepository;

import java.time.LocalTime;

/**
 * Restriction on document sign during some period.
 * There is a period in a day during which nothing can be signed,
 * hence there is no point in editing because it involves signing.
 * This restriction gives no impact on document creation
 *
 * @see RestrictionContext#DEFAULT_DOCUMENT_PROCESSING_TIME_RESTRICTION_FROM
 * @see RestrictionContext#DEFAULT_DOCUMENT_PROCESSING_TIME_RESTRICTION_TO
 * @see RestrictionContext#documentProcessingTimeRestrictionFrom
 * @see RestrictionContext#documentProcessingTimeRestrictionTo
 */
@Getter
@Setter
public class DocumentProcessingTimeRestriction extends Restriction {

    @Override
    public RestrictionResponse isAllowed(RestrictionContext context, Document document, Company signer, DocumentRepository repository) {
        LocalTime now = LocalTime.now();
        LocalTime from = context.getDocumentProcessingTimeRestrictionFrom();
        LocalTime to = context.getDocumentProcessingTimeRestrictionTo();
        boolean isAllowed;
        if (from.isAfter(to)) isAllowed = !(now.isAfter(from) || now.isBefore(to));
        else isAllowed = !(now.isAfter(from) && now.isBefore(to));
        if (!isAllowed) {
            final String unformattedMessage = bundle.getString("document.sign.time.restriction");
            return RestrictionResponse.builder()
                    .allowed(false)
                    .message(String.format(unformattedMessage, from.toString(), to.toString()))
                    .type(RestrictionType.TIME_RESTRICTION)
                    .build();
        }
        return checkNext(context, document, signer, repository);
    }
}
