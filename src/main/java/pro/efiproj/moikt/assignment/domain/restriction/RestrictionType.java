package pro.efiproj.moikt.assignment.domain.restriction;

/**
 * Type representing a concrete restriction
 * in chain. This is for feedback about validation
 * pass/failure, so the code will know how to handle
 * the situation. In case of new restriction, new
 * type may need to be added here. Or some of existing
 * types can be used if handling for it satisfies the case.
 */
public enum RestrictionType {
    TIME_RESTRICTION,
    ACTIVE_DOCUMENT_PROCESSING_RESTRICTION,
    DOCUMENT_CREATION_LIMIT,
    SAME_COMPANY_DOCUMENT_PROCESSING_LIMIT,
    OK
}