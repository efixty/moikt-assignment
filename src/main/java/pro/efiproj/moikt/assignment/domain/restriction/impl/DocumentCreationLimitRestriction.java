package pro.efiproj.moikt.assignment.domain.restriction.impl;

import lombok.Getter;
import lombok.Setter;
import pro.efiproj.moikt.assignment.domain.company.Company;
import pro.efiproj.moikt.assignment.domain.document.Document;
import pro.efiproj.moikt.assignment.domain.restriction.Restriction;
import pro.efiproj.moikt.assignment.domain.restriction.RestrictionContext;
import pro.efiproj.moikt.assignment.domain.restriction.RestrictionResponse;
import pro.efiproj.moikt.assignment.domain.restriction.RestrictionType;
import pro.efiproj.moikt.assignment.repository.document.DocumentRepository;

import java.time.Duration;
import java.util.List;

/**
 * Restriction on document creation for a given company in a given duration.
 * There is a limit for every company on number of document creations in a
 * given duration
 *
 * @see RestrictionContext#DOCUMENT_CREATION_DEFAULT_LIMIT
 * @see RestrictionContext#DOCUMENT_CREATION_DEFAULT_LIMIT_DURING
 * @see RestrictionContext#documentCreationLimit
 * @see RestrictionContext#documentCreationLimitDuring
 */
@Getter
@Setter
public class DocumentCreationLimitRestriction extends Restriction {

    @Override
    public RestrictionResponse isAllowed(RestrictionContext context, Document document, Company signer, DocumentRepository repository) {
        Long creatorId = document.getCreator().getId();
        Duration limitDuration = context.getDocumentCreationLimitDuring();
        int creationLimit = context.getDocumentCreationLimit();
        List<Document> documents = repository.findNotSignedByBothByDuration(creatorId, limitDuration);
        boolean allowed = documents.size() < creationLimit;
        if (!allowed) {
            final String unformattedMessage = bundle.getString("document.creation.not.allowed");
            return RestrictionResponse.builder()
                    .allowed(false)
                    .message(String.format(unformattedMessage, creatorId, document.getCreator().getName()))
                    .type(RestrictionType.DOCUMENT_CREATION_LIMIT)
                    .build();
        }
        return checkNext(context, document, signer, repository);
    }
}
