package pro.efiproj.moikt.assignment.domain.restriction;

import lombok.Getter;
import lombok.Setter;

import java.time.Duration;
import java.time.LocalTime;

/**
 * Context defining the rules of validation. Every parameter
 * should have its default value. Thanks to this context
 * every parameter of restriction can vary during runtime.
 */
@Getter
@Setter
public class RestrictionContext {
    private static final int ACTIVE_DOCUMENT_PROCESSING_DEFAULT_LIMIT = 10;
    private static final int DOCUMENT_CREATION_DEFAULT_LIMIT = 5;
    private static final Duration DOCUMENT_CREATION_DEFAULT_LIMIT_DURING = Duration.ofMinutes(60);
    private static final int SAME_COMPANY_DOCUMENT_PROCESSING_DEFAULT_LIMIT = 2;
    private static final LocalTime DEFAULT_DOCUMENT_PROCESSING_TIME_RESTRICTION_FROM = LocalTime.of(21, 0);
    private static final LocalTime DEFAULT_DOCUMENT_PROCESSING_TIME_RESTRICTION_TO = LocalTime.of(7, 0);

    private int activeDocumentProcessingLimit = ACTIVE_DOCUMENT_PROCESSING_DEFAULT_LIMIT;
    private int documentCreationLimit = DOCUMENT_CREATION_DEFAULT_LIMIT;
    private Duration documentCreationLimitDuring = DOCUMENT_CREATION_DEFAULT_LIMIT_DURING;
    private int sameCompanyDocumentProcessingLimit = SAME_COMPANY_DOCUMENT_PROCESSING_DEFAULT_LIMIT;
    private LocalTime documentProcessingTimeRestrictionFrom = DEFAULT_DOCUMENT_PROCESSING_TIME_RESTRICTION_FROM;
    private LocalTime documentProcessingTimeRestrictionTo = DEFAULT_DOCUMENT_PROCESSING_TIME_RESTRICTION_TO;
}
