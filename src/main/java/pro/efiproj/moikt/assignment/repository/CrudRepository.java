package pro.efiproj.moikt.assignment.repository;

import pro.efiproj.moikt.assignment.repository.document.impl.DocumentRepositoryImpl;

import java.util.List;

/**
 * This interface includes methods every (CRUD) repository has. There is no
 * real database used in this application, but there are imitations of tables for
 * {@link pro.efiproj.moikt.assignment.domain.company.Company},
 * {@link pro.efiproj.moikt.assignment.domain.document.Document}.
 * They are static final hash maps. There is also sequences starting with 0 and
 * incrementing upon new save. DocumentRepository implementation class also provide method for
 * imitating drop of table and sequence (at the same time).
 *
 * @param <T>  java class representing a table
 * @param <ID> java class representing the primary key of table
 * @see pro.efiproj.moikt.assignment.repository.company.CompanyRepository
 * @see pro.efiproj.moikt.assignment.repository.company.impl.CompanyRepositoryImpl#db
 * @see pro.efiproj.moikt.assignment.repository.company.impl.CompanyRepositoryImpl#sequence
 * @see pro.efiproj.moikt.assignment.repository.document.DocumentRepository
 * @see pro.efiproj.moikt.assignment.repository.document.impl.DocumentRepositoryImpl#db
 * @see pro.efiproj.moikt.assignment.repository.document.impl.DocumentRepositoryImpl#sequence
 * @see DocumentRepositoryImpl#dropDbAndSequence()
 */
public interface CrudRepository<T, ID> {
    T findById(ID id);

    List<T> findAll();

    T save(T entity);

    T deleteById(ID id);
}
