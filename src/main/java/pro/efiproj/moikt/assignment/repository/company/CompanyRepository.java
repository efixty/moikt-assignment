package pro.efiproj.moikt.assignment.repository.company;

import pro.efiproj.moikt.assignment.domain.company.Company;
import pro.efiproj.moikt.assignment.repository.CrudRepository;

/**
 * Repository created as similar as possible to spring data-jpa repository
 * for easy integration with an existing rest application
 */
public interface CompanyRepository extends CrudRepository<Company, Long> {
}
