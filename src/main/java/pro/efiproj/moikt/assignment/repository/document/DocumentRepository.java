package pro.efiproj.moikt.assignment.repository.document;

import pro.efiproj.moikt.assignment.domain.document.Document;
import pro.efiproj.moikt.assignment.repository.CrudRepository;

import java.time.Duration;
import java.util.List;

/**
 * Repository created as similar as possible to spring data-jpa repository
 * for easy integration with an existing rest application
 */
public interface DocumentRepository extends CrudRepository<Document, Long> {
    /**
     * Finds documents that does not have SIGNED_BY_BOTH
     * status (those having either NEWLY_CREATED or IN_PROCESS)
     * and whose companies are those from given document
     *
     * @param document containing companies for filtering
     * @return list of documents satisfying described condition
     */
    List<Document> findNotSignedByBothByDocument(Document document);

    /**
     * Finds documents that does not have SIGNED_BY_BOTH
     * status (those having either NEWLY_CREATED or IN_PROCESS),
     * created after <code>LocalDateTime.now().minus(limitDuration)</code>
     * and whose creator has the given id
     *
     * @param creatorId     id of creator of searched documents
     * @param limitDuration duration since earliest wanted document creation until now
     * @return list of documents satisfying described condition
     */
    List<Document> findNotSignedByBothByDuration(Long creatorId, Duration limitDuration);

    /**
     * Finds documents that does not have SIGNED_BY_BOTH
     * status (those having either NEWLY_CREATED or IN_PROCESS)
     * and company with given id is involved in their processing
     *
     * @param signerId id of company that involved in processing of documents queried
     * @return list of documents satisfying described condition
     */
    List<Document> findNotSignedByBothWithGivenSigner(Long signerId);
}
