package pro.efiproj.moikt.assignment.repository.company.impl;

import pro.efiproj.moikt.assignment.domain.company.Company;
import pro.efiproj.moikt.assignment.repository.company.CompanyRepository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CompanyRepositoryImpl implements CompanyRepository {

    private static final Map<Long, Company> db = new HashMap<>();
    private static Long sequence = 0L;

    @Override
    public Company findById(Long id) {
        return db.get(id);
    }

    @Override
    public List<Company> findAll() {
        return new ArrayList<>(db.values());
    }

    @Override
    public Company save(Company company) {
        if (company.getId() == null || db.get(company.getId()) == null) {
            company.setId(sequence++);
        }
        Long newCompanyId = company.getId();
        db.put(company.getId(), company);
        return findById(newCompanyId);
    }

    @Override
    public Company deleteById(Long id) {
        return db.remove(id);
    }
}
