package pro.efiproj.moikt.assignment.repository.document.impl;

import pro.efiproj.moikt.assignment.domain.document.Document;
import pro.efiproj.moikt.assignment.domain.document.DocumentProcessStatus;
import pro.efiproj.moikt.assignment.repository.document.DocumentRepository;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DocumentRepositoryImpl implements DocumentRepository {

    private static final Map<Long, Document> db = new HashMap<>();
    private static Long sequence = 0L;

    public static void dropDbAndSequence() {
        db.clear();
        sequence = 0L;
    }

    @Override
    public List<Document> findNotSignedByBothByDocument(Document document) {
        Long creatorId = document.getCreator().getId();
        Long otherId = document.getOther().getId();
        return db.values()
                .stream()
                .filter(doc -> isCreator(doc, creatorId) || isOther(doc, creatorId))
                .filter(doc -> isCreator(doc, otherId) || isOther(doc, otherId))
                .filter(this::notSignedByBoth)
                .collect(Collectors.toList());
    }

    @Override
    public List<Document> findNotSignedByBothByDuration(Long creatorId, Duration limitDuration) {
        return db.values()
                .stream()
                .filter(doc -> doc.getCreationDate().isAfter(LocalDateTime.now().minus(limitDuration)))
                .filter(doc -> isCreator(doc, creatorId))
                .filter(this::notSignedByBoth)
                .collect(Collectors.toList());
    }

    @Override
    public List<Document> findNotSignedByBothWithGivenSigner(Long signerId) {
        return db.values()
                .stream()
                .filter(doc -> isCreator(doc, signerId) || isOther(doc, signerId))
                .filter(this::notSignedByBoth)
                .collect(Collectors.toList());
    }

    @Override
    public Document findById(Long id) {
        return db.get(id);
    }

    @Override
    public List<Document> findAll() {
        return new ArrayList<>(db.values());
    }

    @Override
    public Document save(Document document) {
        if (document.getId() == null || db.get(document.getId()) == null) {
            document.setId(sequence++);
        }
        Long newDocumentId = document.getId();
        db.put(document.getId(), document);
        return findById(newDocumentId);
    }

    @Override
    public Document deleteById(Long id) {
        return db.remove(id);
    }

    private boolean isCreator(Document document, Long possibleCreator) {
        return document.getCreator().getId().equals(possibleCreator);
    }

    private boolean isOther(Document document, Long possibleOther) {
        return document.getOther().getId().equals(possibleOther);
    }

    private boolean notSignedByBoth(Document document) {
        return document.getStatus() != DocumentProcessStatus.SIGNED_BY_BOTH;
    }
}
