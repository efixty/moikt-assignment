package pro.efiproj.moikt.assignment.service;

import pro.efiproj.moikt.assignment.domain.document.Document;

/**
 * This class contains the interface for document processing
 * @see pro.efiproj.moikt.assignment.service.impl.DocumentProcessingServiceImpl
 */
public interface DocumentProcessingService {
    /**
     * Attempts to create document with given document content between companies with given ids. That
     * document gains {@link pro.efiproj.moikt.assignment.domain.document.DocumentProcessStatus#NEWLY_CREATED}
     * status. If document creation is successful then attempts to sign the document on creator's behalf. Document
     * gains {@link pro.efiproj.moikt.assignment.domain.document.DocumentProcessStatus#IN_PROCESS} status
     *
     * @param creatorId    id of creator of the document
     * @param otherId      id of other signer of document
     * @param documentText content of document
     * @return created document
     */
    Document createDocument(Long creatorId, Long otherId, String documentText);

    /**
     * Attempts to edit document with given id, changing its content to given new content and
     * then signing document on behalf of company with given id.
     * The sign of other company is discarded
     *
     * @param documentId id of document to be edited
     * @param editorId   id of editor company
     * @param newText    new content for document
     * @return edited document
     */
    Document editDocument(Long documentId, Long editorId, String newText);

    /**
     * Attempts to sign document with given id on behalf of company with given id.
     * If it is the seconds sign on the document then document's status becomes
     * {@link pro.efiproj.moikt.assignment.domain.document.DocumentProcessStatus#SIGNED_BY_BOTH}
     *
     * @param documentId id of document to be signed
     * @param signerId id signer company
     * @return signed document
     */
    Document signDocument(Long documentId, Long signerId);
}
