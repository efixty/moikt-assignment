package pro.efiproj.moikt.assignment.service.impl;

import pro.efiproj.moikt.assignment.config.ResourceBundleProvider;
import pro.efiproj.moikt.assignment.domain.company.Company;
import pro.efiproj.moikt.assignment.domain.document.Document;
import pro.efiproj.moikt.assignment.domain.document.DocumentProcessStatus;
import pro.efiproj.moikt.assignment.domain.restriction.Restriction;
import pro.efiproj.moikt.assignment.domain.restriction.RestrictionContext;
import pro.efiproj.moikt.assignment.domain.restriction.RestrictionResponse;
import pro.efiproj.moikt.assignment.exception.document.DocumentCreationException;
import pro.efiproj.moikt.assignment.exception.document.DocumentSignException;
import pro.efiproj.moikt.assignment.repository.company.CompanyRepository;
import pro.efiproj.moikt.assignment.repository.company.impl.CompanyRepositoryImpl;
import pro.efiproj.moikt.assignment.repository.document.DocumentRepository;
import pro.efiproj.moikt.assignment.repository.document.impl.DocumentRepositoryImpl;
import pro.efiproj.moikt.assignment.service.DocumentProcessingService;

import java.util.ResourceBundle;

/**
 * Implementation of document processing service. This is the service layer of a
 * rest application. Here is the logic of document processing
 */
public class DocumentProcessingServiceImpl implements DocumentProcessingService {

    private final ResourceBundle errorMessages;
    private final DocumentRepository documentRepository;
    private final CompanyRepository companyRepository;

    /**
     * Restriction context containing rules of validation
     */
    private final RestrictionContext restrictionContext;

    /**
     * Chain for document creation validation
     */
    private final Restriction documentCreateRestrictions;

    /**
     * Chain for document edit/sign validation
     */
    private final Restriction documentSignRestrictions;

    public DocumentProcessingServiceImpl(Restriction documentCreateRestrictions,
                                         Restriction documentSignRestrictions,
                                         RestrictionContext restrictionContext) {
        this.documentCreateRestrictions = documentCreateRestrictions;
        this.documentSignRestrictions = documentSignRestrictions;
        this.restrictionContext = restrictionContext;
        this.documentRepository = new DocumentRepositoryImpl();
        this.companyRepository = new CompanyRepositoryImpl();
        this.errorMessages = ResourceBundleProvider.getResourceBundleForName("error_messages");
    }

    @Override
    public Document createDocument(Long creatorId, Long otherId, String documentText) {
        validateCompanyIdsForDocumentCreation(creatorId, otherId);
        Company creator = companyRepository.findById(creatorId);
        Company other = companyRepository.findById(otherId);
        Document created = new Document();
        created.setCreator(creator);
        created.setOther(other);
        created.setDocumentText(documentText);
        addDocument(created);
        return created;
    }

    @Override
    public Document editDocument(Long documentId, Long editorId, String newText) {
        Document toEdit = documentRepository.findById(documentId);
        toEdit.setDocumentText(newText);
        return signDocument(toEdit, editorId, true);
    }

    @Override
    public Document signDocument(Long documentId, Long signerId) {
        Document toSign = documentRepository.findById(documentId);
        return signDocument(toSign, signerId, false);
    }

    /**
     * Validates document creation and, if successful, attempts to sign the document on behalf of creator.
     * On every successful step document is persisted
     *
     * @param document newly created document
     * @return final document
     */
    private Document addDocument(Document document) {
        RestrictionResponse response = documentCreateRestrictions
                .isAllowed(restrictionContext, document, document.getCreator(), documentRepository);
        if (response.isAllowed()) {
            Document processed = documentRepository.save(document);
            processed.setStatus(DocumentProcessStatus.IN_PROCESS);
            signDocument(processed, processed.getCreator().getId(), false);
            processed = documentRepository.save(document);
            return processed;
        } else {
            String errorMsg = response.getMessage();
            throw new DocumentCreationException(String.format(errorMsg, document.getCreator().getId()));
        }
    }

    /**
     * Signs document on behalf of company with given id. If 'isEditing' is set to true
     * then existing signature will be discarded.
     *
     * @param toSign    document to be signed/edited
     * @param signerId  id of signer/editor
     * @param isEditing switch for signing/editing
     * @return final document
     */
    private Document signDocument(Document toSign, Long signerId, boolean isEditing) {
        validateProcessedDocumentStatus(toSign);
        if (signerId.equals(toSign.getCreator().getId())) return signCreator(toSign, isEditing);
        else if (signerId.equals(toSign.getOther().getId())) return signOther(toSign, isEditing);
        else {
            final String errorMsg = errorMessages.getString("document.sign.unknown.company");
            throw new DocumentSignException(String.format(errorMsg, signerId, toSign.getId()));
        }
    }

    /**
     * Validates company ids for document creation.
     * Ids must be non-null different values, both contained in database.
     *
     * @param creatorId id of creator company
     * @param otherId   id of other company
     */
    private void validateCompanyIdsForDocumentCreation(Long creatorId, Long otherId) {
        if (creatorId == null || companyRepository.findById(creatorId) == null) {
            final String errorMsg = errorMessages.getString("company.id.not.valid");
            throw new DocumentCreationException(String.format(errorMsg, creatorId));
        }
        if (otherId == null) {
            final String errorMsg = errorMessages.getString("company.id.not.valid");
            throw new DocumentCreationException(String.format(errorMsg, otherId));
        }
        if (creatorId.equals(otherId)) {
            final String errorMsg = errorMessages.getString("document.creation.ids.same");
            throw new DocumentCreationException(String.format(errorMsg, otherId));
        }
        if (companyRepository.findById(otherId) == null) {
            final String errorMsg = errorMessages.getString("company.id.not.valid");
            throw new DocumentCreationException(String.format(errorMsg, otherId));
        }
    }

    private void validateProcessedDocumentStatus(Document toSign) {
        if (toSign.getStatus() != DocumentProcessStatus.IN_PROCESS) {
            final String errorMsg = errorMessages.getString("document.sign.process.not.active");
            throw new DocumentSignException(String.format(errorMsg, toSign.getId(), toSign.getStatus().name()));
        }
    }

    /**
     * After validating the sign ability, signs the document on behalf of creator
     * If another sign is present and 'isEditing' is set to false then document
     * processing ended for this document.
     * Document's status becomes {@link DocumentProcessStatus#SIGNED_BY_BOTH}
     * If 'isEditing' is set to true then any signature is discarded.
     *
     * @param toSign    document to be signed on behalf of creator
     * @param isEditing switches between editing and signing modes
     * @return final document
     */
    private Document signCreator(Document toSign, boolean isEditing) {
        Company signer = toSign.getCreator();
        if (toSign.isSignedByCreator()) {
            final String errorMsg = errorMessages.getString("document.sign.already.signed");
            throw new DocumentSignException(String.format(errorMsg, toSign.getId(), signer.getId()));
        }
        RestrictionResponse response = documentSignRestrictions
                .isAllowed(restrictionContext, toSign, signer, documentRepository);
        if (response.isAllowed()) {
            toSign.setSignedByCreator(true);
            if (isEditing) toSign.setSignedByOther(false);
            else if (toSign.isSignedByOther()) toSign.setStatus(DocumentProcessStatus.SIGNED_BY_BOTH);
            return documentRepository.save(toSign);
        } else {
            final String errorMsg = response.getMessage();
            throw new DocumentSignException(String.format(errorMsg, signer.getId()));
        }
    }

    /**
     * After validating sign ability, signs the document on behalf of other
     * If another sign is present and 'isEditing' is set to false then document
     * processing ended for this document.
     * Document's status becomes {@link DocumentProcessStatus#SIGNED_BY_BOTH}
     * If 'isEditing' is set to true then any sign is discarded.
     *
     * @param toSign    document to be signed on behalf of other
     * @param isEditing switches between editing and signing modes
     * @return final document
     */
    private Document signOther(Document toSign, boolean isEditing) {
        Company signer = toSign.getOther();
        if (toSign.isSignedByOther()) {
            final String errorMsg = errorMessages.getString("document.sign.already.signed");
            throw new DocumentSignException(String.format(errorMsg, toSign.getId(), signer.getId()));
        }
        RestrictionResponse response = documentSignRestrictions
                .isAllowed(restrictionContext, toSign, signer, documentRepository);
        if (response.isAllowed()) {
            toSign.setSignedByOther(true);
            if (isEditing) toSign.setSignedByCreator(false);
            else if (toSign.isSignedByCreator()) toSign.setStatus(DocumentProcessStatus.SIGNED_BY_BOTH);
            return documentRepository.save(toSign);
        } else {
            final String errorMsg = response.getMessage();
            throw new DocumentSignException(String.format(errorMsg, signer.getId()));
        }
    }
}
