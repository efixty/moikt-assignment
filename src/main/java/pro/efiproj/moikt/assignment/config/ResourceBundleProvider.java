package pro.efiproj.moikt.assignment.config;

import java.util.ResourceBundle;

/**
 * As the application is meant to be easy to inject in already existing
 * rest application (as a service, middle layer), this class is created
 * to be marked with @Configuration annotation (spring configuration).
 * Its only method can be then marked with @Bean annotation so that
 * resource bundle can be injected everywhere needed.
 */
public class ResourceBundleProvider {

    /**
     * Provides resource bundles with given name
     *
     * @param bundleName name of resource bundle to find
     * @return found resource bundle
     */
    public static ResourceBundle getResourceBundleForName(String bundleName) {
        return ResourceBundle.getBundle(bundleName);
    }
}
