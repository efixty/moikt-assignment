package pro.efiproj.moikt.assignment.exception.document;

import lombok.Getter;

/**
 * Exception that is thrown in case something goes
 * wrong during document sign
 *
 * @see pro.efiproj.moikt.assignment.domain.restriction.impl.DocumentProcessingTimeRestriction
 */
@Getter
public class DocumentSignException extends RuntimeException {
    private final String message;

    public DocumentSignException(String message) {
        super(message);
        this.message = message;
    }
}
