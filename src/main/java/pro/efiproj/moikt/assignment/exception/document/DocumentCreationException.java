package pro.efiproj.moikt.assignment.exception.document;

import lombok.Getter;

/**
 * Exception that is thrown in case something goes
 * wrong during document creation
 *
 * @see pro.efiproj.moikt.assignment.domain.restriction.impl.DocumentCreationLimitRestriction
 * @see pro.efiproj.moikt.assignment.domain.restriction.impl.SameCompanyDocumentProcessingLimit
 * @see pro.efiproj.moikt.assignment.domain.restriction.impl.ActiveDocumentProcessingLimit
 */
@Getter
public class DocumentCreationException extends RuntimeException {
    private final String message;

    public DocumentCreationException(String message) {
        super(message);
        this.message = message;
    }
}
