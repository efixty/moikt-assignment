package pro.efiproj.moikt.assignment;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import pro.efiproj.moikt.assignment.domain.company.Company;
import pro.efiproj.moikt.assignment.domain.document.Document;
import pro.efiproj.moikt.assignment.domain.document.DocumentProcessStatus;
import pro.efiproj.moikt.assignment.domain.restriction.Restriction;
import pro.efiproj.moikt.assignment.domain.restriction.RestrictionContext;
import pro.efiproj.moikt.assignment.domain.restriction.impl.ActiveDocumentProcessingLimit;
import pro.efiproj.moikt.assignment.domain.restriction.impl.DocumentCreationLimitRestriction;
import pro.efiproj.moikt.assignment.domain.restriction.impl.DocumentProcessingTimeRestriction;
import pro.efiproj.moikt.assignment.domain.restriction.impl.SameCompanyDocumentProcessingLimit;
import pro.efiproj.moikt.assignment.exception.document.DocumentCreationException;
import pro.efiproj.moikt.assignment.exception.document.DocumentSignException;
import pro.efiproj.moikt.assignment.repository.company.CompanyRepository;
import pro.efiproj.moikt.assignment.repository.company.impl.CompanyRepositoryImpl;
import pro.efiproj.moikt.assignment.repository.document.impl.DocumentRepositoryImpl;
import pro.efiproj.moikt.assignment.service.DocumentProcessingService;
import pro.efiproj.moikt.assignment.service.impl.DocumentProcessingServiceImpl;

import java.time.LocalTime;

public class DocumentProcessingFlowTest {

    private RestrictionContext context;
    private DocumentProcessingService service;
    private CompanyRepository companyRepository;
    private Restriction documentCreationRestrictions;
    private Restriction documentSignRestrictions;

    @Before
    public void setup() {
        this.context = new RestrictionContext();
        // There are separate tests for every restriction case and context modification
        // Here are just flow tests, so we suppose restrictions are OK for all tests in this class
        makeContextParametersSafe();
        this.companyRepository = new CompanyRepositoryImpl();
        initializeRestrictions();
        this.service = new DocumentProcessingServiceImpl(documentCreationRestrictions, documentSignRestrictions, context);
        initializeCompanies();
    }

    private void makeContextParametersSafe() {
        context.setDocumentProcessingTimeRestrictionFrom(LocalTime.now().plusHours(1));
        context.setDocumentProcessingTimeRestrictionTo(LocalTime.now().plusHours(2));
        context.setSameCompanyDocumentProcessingLimit(15);
        context.setActiveDocumentProcessingLimit(15);
        context.setDocumentCreationLimit(15);
    }

    private void initializeRestrictions() {
        this.documentCreationRestrictions = new DocumentCreationLimitRestriction();
        this.documentCreationRestrictions.addNext(new SameCompanyDocumentProcessingLimit())
                .addNext(new ActiveDocumentProcessingLimit());
        this.documentSignRestrictions = new DocumentProcessingTimeRestriction();
    }

    private void initializeCompanies() {
        Company companyA = new Company();
        companyA.setName("Company A");
        companyRepository.save(companyA);

        Company companyB = new Company();
        companyB.setName("Company B");
        companyRepository.save(companyB);

        Company companyC = new Company();
        companyC.setName("Company C");
        companyRepository.save(companyC);
    }

    @Test
    public void noEditFlowTest() {
        final String documentText = "text";
        final Document expected = new Document();
        expected.setId(0L);
        expected.setCreator(companyRepository.findById(0L));
        expected.setOther(companyRepository.findById(1L));
        expected.setSignedByCreator(true);
        expected.setSignedByOther(true);
        expected.setDocumentText(documentText);
        expected.setStatus(DocumentProcessStatus.SIGNED_BY_BOTH);

        Document result = service.createDocument(0L, 1L, documentText);
        // Notification or email went to Company B that their company has documents to sign
        // Some sort of link or something is present in notification
        // Representative follows link, logs in. Then examines the document's current state and sign the document
        result = service.signDocument(result.getId(), 1L);

        Assert.assertEquals(expected, result);
    }

    @Test
    public void editFlowTest() {
        final String documentCreationText = "creation text";
        final String documentEditText = "edit text";
        final Document expected = new Document();
        expected.setId(0L);
        expected.setCreator(companyRepository.findById(0L));
        expected.setOther(companyRepository.findById(1L));
        expected.setSignedByCreator(true);
        expected.setSignedByOther(true);
        expected.setDocumentText(documentEditText);
        expected.setStatus(DocumentProcessStatus.SIGNED_BY_BOTH);

        Document result = service.createDocument(0L, 1L, documentCreationText);
        // Notification or email went to Company B that their company has documents to sign
        // Some sort of link or something is present in notification
        // Representative follows link, logs in. Then examines the document's current state and sign the document
        result = service.editDocument(result.getId(), 1L, documentEditText);
        // Another notification, but now to the Company A
        // They decide to sign this
        result = service.signDocument(result.getId(), 0L);

        Assert.assertEquals(expected, result);
    }

    @Test(expected = DocumentCreationException.class)
    public void nullCreatorTest() {
        final String documentText = "text";
        service.createDocument(null, 0L, documentText);
    }

    @Test(expected = DocumentCreationException.class)
    public void nullOtherTest() {
        final String documentText = "text";
        service.createDocument(0L, null, documentText);
    }

    @Test(expected = DocumentCreationException.class)
    public void sameCreatorOtherTest() {
        final String documentText = "text";
        service.createDocument(0L, 0L, documentText);
    }

    @Test
    public void partialNoEditFlowTest() {
        final String documentText = "text";
        final Document expected = new Document();
        expected.setId(0L);
        expected.setCreator(companyRepository.findById(0L));
        expected.setOther(companyRepository.findById(1L));
        expected.setSignedByCreator(true);
        expected.setSignedByOther(false);
        expected.setDocumentText(documentText);
        expected.setStatus(DocumentProcessStatus.IN_PROCESS);

        final Document result = service.createDocument(0L, 1L, documentText);

        Assert.assertEquals(expected, result);
    }

    @Test
    public void partialEditFlowTest() {
        final String documentCreationText = "creation text";
        final String documentEditText = "edit text";
        final Document expected = new Document();
        expected.setId(0L);
        expected.setCreator(companyRepository.findById(0L));
        expected.setOther(companyRepository.findById(1L));
        expected.setSignedByCreator(false);
        expected.setSignedByOther(true);
        expected.setDocumentText(documentEditText);
        expected.setStatus(DocumentProcessStatus.IN_PROCESS);

        Document result = service.createDocument(0L, 1L, documentCreationText);
        result = service.editDocument(result.getId(), 1L, documentEditText);

        Assert.assertEquals(expected, result);
    }

    @Test(expected = DocumentSignException.class)
    public void nonInvolvedCompanySignAttempt() {
        final String documentText = "text";
        final Document result = service.createDocument(0L, 1L, documentText);
        service.signDocument(result.getId(), 2L);
    }

    @Test(expected = DocumentSignException.class)
    public void alreadySignedCompanySignAttempt() {
        final String documentText = "text";
        final Document result = service.createDocument(0L, 1L, documentText);
        service.signDocument(result.getId(), 0L);
    }

    @After
    public void clearDb() {
        DocumentRepositoryImpl.dropDbAndSequence();
    }
}
