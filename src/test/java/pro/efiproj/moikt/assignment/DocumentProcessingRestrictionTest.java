package pro.efiproj.moikt.assignment;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import pro.efiproj.moikt.assignment.domain.company.Company;
import pro.efiproj.moikt.assignment.domain.document.Document;
import pro.efiproj.moikt.assignment.domain.restriction.Restriction;
import pro.efiproj.moikt.assignment.domain.restriction.RestrictionContext;
import pro.efiproj.moikt.assignment.domain.restriction.impl.ActiveDocumentProcessingLimit;
import pro.efiproj.moikt.assignment.domain.restriction.impl.DocumentCreationLimitRestriction;
import pro.efiproj.moikt.assignment.domain.restriction.impl.DocumentProcessingTimeRestriction;
import pro.efiproj.moikt.assignment.domain.restriction.impl.SameCompanyDocumentProcessingLimit;
import pro.efiproj.moikt.assignment.exception.document.DocumentCreationException;
import pro.efiproj.moikt.assignment.exception.document.DocumentSignException;
import pro.efiproj.moikt.assignment.repository.company.CompanyRepository;
import pro.efiproj.moikt.assignment.repository.company.impl.CompanyRepositoryImpl;
import pro.efiproj.moikt.assignment.repository.document.impl.DocumentRepositoryImpl;
import pro.efiproj.moikt.assignment.service.DocumentProcessingService;
import pro.efiproj.moikt.assignment.service.impl.DocumentProcessingServiceImpl;

import java.time.Duration;
import java.time.LocalTime;

public class DocumentProcessingRestrictionTest {

    private RestrictionContext context;
    private DocumentProcessingService service;
    private CompanyRepository companyRepository;
    private Restriction documentCreationRestrictions;
    private Restriction documentSignRestrictions;

    @Before
    public void setup() {
        this.context = new RestrictionContext();
        this.companyRepository = new CompanyRepositoryImpl();
        initializeRestrictions();
        this.service = new DocumentProcessingServiceImpl(documentCreationRestrictions, documentSignRestrictions, context);
        initializeCompanies();
    }

    private void initializeRestrictions() {
        this.documentCreationRestrictions = new DocumentCreationLimitRestriction();
        this.documentCreationRestrictions.addNext(new SameCompanyDocumentProcessingLimit())
                .addNext(new ActiveDocumentProcessingLimit());
        this.documentSignRestrictions = new DocumentProcessingTimeRestriction();
    }

    private void initializeCompanies() {
        Company companyA = new Company();
        companyA.setName("Company A");
        companyRepository.save(companyA);

        Company companyB = new Company();
        companyB.setName("Company B");
        companyRepository.save(companyB);

        Company companyC = new Company();
        companyC.setName("Company C");
        companyRepository.save(companyC);
    }

    @Test(expected = DocumentCreationException.class)
    public void documentCreationLimitRestrictionTest() {
        final String documentText = "text";
        context.setDocumentCreationLimit(4); // Can create maximum of 4 documents...
        context.setDocumentCreationLimitDuring(Duration.ofMinutes(5)); // during 5 minutes

        service.createDocument(0L, 1L, documentText);
        service.createDocument(0L, 1L, documentText);
        service.createDocument(0L, 1L, documentText);
        service.createDocument(0L, 1L, documentText);
        service.createDocument(0L, 1L, documentText);
    }

    @Test(expected = DocumentCreationException.class)
    public void sameCompanyDocumentProcessingRestrictionWithInProcessStatusTest() {
        final String documentText = "text";
        // There can be maximum of 2 IN_PROCESS or NEWLY_CREATED document processing between same companies
        context.setSameCompanyDocumentProcessingLimit(2);

        service.createDocument(0L, 1L, documentText);
        service.createDocument(0L, 1L, documentText);
        service.createDocument(1L, 0L, documentText);
    }

    @Test
    public void sameCompanyDocumentProcessingRestrictionWithSignedStatusTest() {
        final String documentText = "text";
        context.setSameCompanyDocumentProcessingLimit(2);

        service.createDocument(0L, 1L, documentText);
        final Document toBeSigned = service.createDocument(0L, 1L, documentText);
        service.signDocument(toBeSigned.getId(), 1L); // Status changed to SIGNED_BY_BOTH
        service.createDocument(1L, 0L, documentText);
    }

    @Test(expected = DocumentCreationException.class)
    public void activeDocumentProcessingRestrictionTest() {
        final String documentText = "text";

        // This is done to ensure that active document processing restriction has been called
        context.setSameCompanyDocumentProcessingLimit(3);
        context.setDocumentCreationLimit(6);

        // Same company can be involved in maximum of 4 document processing with status IN_PROCESS or NEWLY_CREATED
        context.setActiveDocumentProcessingLimit(4);

        service.createDocument(0L, 1L, documentText);
        service.createDocument(1L, 0L, documentText);
        service.createDocument(2L, 0L, documentText);
        service.createDocument(0L, 2L, documentText);
        service.createDocument(0L, 2L, documentText);
    }

    @Test(expected = DocumentSignException.class)
    public void documentProcessingTimeRestrictionTest() {
        final String documentText = "text";

        // Documents cannot be signed from one hour before now
        context.setDocumentProcessingTimeRestrictionFrom(LocalTime.now().minusHours(1));

        // Until one hour after now
        context.setDocumentProcessingTimeRestrictionTo(LocalTime.now().plusHours(1));

        // Notice that we expect sign exception as document gets signed by creator upon creation
        service.createDocument(0L, 1L, documentText);
    }

    @After
    public void invalidateState() {
        this.context = new RestrictionContext();
        DocumentRepositoryImpl.dropDbAndSequence();
    }
}
